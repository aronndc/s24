console.log("Hello World!");

// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:

const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube} `);


	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:

const address = {
    street: "Ayala Ave,",
    city: "Makati City,",
    region: "National Capital Region"
}

const { street, city, region } = address;
/*console.log(street);
console.log(city);
console.log(region);*/

function showAddress(address) {
    console.log(`My address is ${street} ${city} ${region}`);
}

showAddress(address);


	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

const animal = {
    name: "Lolong",
    type: "salwater crocodile",
    weigth: "1075 kgs",
    height: "20 ft 3 in"
}

const { name, type, weigth, height } = animal;

function showAnimnal(animal) {
    console.log(`${name} was a ${type}. He weighed at ${weigth} with a measurement of ${height}`)
}
showAnimnal();


	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => {
    console.log(`${numbers}`)
})

const addition = (a, b, c, d, e) => a + b + c + d + e

const total = addition(1, 2, 3, 4, 5);
console.log("Total of the 5 numbers above is: " + total);



/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:

class pet {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const mypet = new pet()
console.log(mypet);

mypet.name = "Chase"
mypet.age = 2
mypet.breed = "Siberian Husky"

console.log(mypet);