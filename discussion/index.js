console.log("Hello World!");

// ES6 Updates:

// Exponent operator: we use "**" for exponents

const firstNum = Math.pow(8, 2);
console.log(firstNum);

const secondNum = 8 ** 2;
console.log(secondNum);

// 5 raised to the power of 5
const thirdNum = 5 ** 5
console.log(thirdNum);

// Template Literals

/*
	Allows us to write strings without using the concatenation operator (+)
*/

let name = "Louies"

// Concatenation / Pre-Template Literal
// Using single Quote (' ')

let message = 'Hello ' + name + '. Welcome to Zuitt Coding Bootcamp!'

console.log("Message without template literal:")
console.log(message);

console.log(" ");

// Strings Using Template Literal
// uses the backticks (` `)
message = `Hello ${name}. Welcome to Zuitt Coding Bootcamp!`
console.log(`Message with template literal: ${message} `);

let anotherMessage = `
${name} attend a math competition.
He won it by solving the problem 8**2 with the solution of ${secondNum}`;

console.log(anotherMessage);

anotherMessage = "\n " + name + ' attended a math competition. \n He won it by solving the problem 8**2 with the solution ' + secondNum + ' \n';

console.log(anotherMessage);

// Operation inside template literal
const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings is: ${principal * interestRate}`);

// Array Destructuring

/*
	Allows to unpack elements in an array into distinct variables. Allows us to name the array elements with variables instead of index number

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! Its nice to meet you!`);


console.log(" ");

// Array Destructuring

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// Object Destructuring

/*
	Allows us to  unnpack properties of objects into distinct variable. Shortends the syntax for accessing properties from objects.

	Syntax:
		curly braces{} if OBJECT

		let/const {propertyName, propertyName, propertyName} = object
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}


// Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

function getFullName(givenName, maidenName, familyName) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person.givenName, person.maidenName, person.familyName);

console.log(" ");

// Using Object Destructuring
const {maidenName, familyName, givenName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName(givenName, maidenName, familyName) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}


getFullName(givenName, maidenName, familyName);

// Mini Activity:

	/*Item 1.)
		- Create a variable employees with a value of an array containing names of employees
		- Destructure the array and print out a message with the names of employees using Template Literals.*/


	// Code here:

console.log(" ");

const employees = {
    employee1: "Qwerty",
    employee2: "Asdfgh",
    employee3: "Zxcvbn"
}

const { employee1, employee2, employee3 } = employees;
console.log(employee1);
console.log(employee2);
console.log(employee3);

function getEmployees(employees) {
    console.log(`${employee1} ${employee2} ${employee3}`)
};


getEmployees(employees);



	/*Item 2.)
		- Create a variable pet with a value of an object data type with different details of your pet as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

const myPet = {
    name: "Chase",
    breed: "Siberian Husky",
    gender: "Male",
    age: 2
}

function getPet(myPet) {
    const { name, breed, gender, age } = myPet;
    console.log(`My pet is
		name: ${name}
		breed: ${breed}
		gender: ${gender}
		age: ${age}`);
}

getPet(myPet);


// Pre-Arrow Functions and Arrow Functions

// Pre-Arrow Function

/*
	Syntax:
		function functionName(paramA, paramB) {
			statement // console.log // return
		}
*/

function printFullName(firstName, middleInitial, lastName) {
	console.log(firstName + " " + middleInitial + " " + lastName);
}
printFullName("Louies Aronn", "P.", "Dela Cerna");

// Arrow Function

/*
	Syntax:
	let/const variableName = (paramA, paramB) => {
		statement // console.log // return
	}
*/

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName} `)
}

printFullName1("Chase", "Maze", "Haste");

const students = ["Yoby", "Emman", "Ronel"];

// Functions with loop

// Pre-Arrow Function

students.forEach(function(students) {
	console.log(students + " is a student.")
})

console.log(" ");

students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statement

console.log(" ")

// pre-Arrow
function add(x,y) {
	return x + y
}

let total = add(12, 15);
console.log(total);

// Arrow Function
const addition = (x, y) => x + y

/*const addition =(x,y) => {
	return x + y
}*/

let total2 = addition(12, 12);
console.log(total2);

// Default Function Argument Value

const greet = (name = "User") => {
	return `Good evening, ${name}.`
}

console.log(greet());
console.log(greet("Archie"));

// Class-Based Object Blueprint

/*
	Allows creation/instantiation of objects using class as blueprint

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor"
myCar.year = 2022

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2022);
console.log(myNewCar)























